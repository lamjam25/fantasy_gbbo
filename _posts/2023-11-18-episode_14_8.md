---
title: "Episode 8 - Party Week"
layout: post
cover-img: "/assets/images/chocolate_cake.png"
published: false
---

Josh's collection of silver medals has to be massive at this point.

# Episode Recap
Cristy is gone

# Scores By Team
{% include episode_14_7_scoring_by_team.html %}
# Scores By Baker
{% include episode_14_7_scoring_by_baker.html %}
# Table as of Episode 7
{% include episode_14_7_agg_table.html %}