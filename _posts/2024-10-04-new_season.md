---
title: "Episode 2 - Biscuit Week"
layout: post
cover-img: "/assets/images/chocolate_cake.png"
---
My my my are these scores juiced!

Now in typical, competent circumstances bakers if I saw so many of you select the same people to win star baker and to leave the tent then I would suspect deviancy along the likes of collusion or conspiracy. 

These were not typical competent circumstances.

Hazel simply had to go. She stayed alive on technicality last week and stood out in all the wrong ways in the showstopper, using short bread (SHORT BREAD)
in a biscuit bake requiring construction. That decision really "punched" her chances in a way even dame "Judy" dench would show no sympathy towards.

In other news Sumayah shined again, making fondant flow like silk on her wondrous showstopper that even moved. Time will tell if she can keep this up.

# Scores By Team
<table style="width: 100%;">
{% for row in site.data.series_15_episode_02_team_scores %}
    {% if forloop.first %}
    <tr>
      {% for pair in row %}
        <th>{{ pair[0] }}</th>
      {% endfor %}
    </tr>
    {% endif %}

    {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
{% endfor %}
</table>

# Fixtures Results for Episode 2
<table style="width: 100%;">
{% for row in site.data.series_15_episode_02_fixtures %}
    {% if forloop.first %}
    <tr>
      {% for pair in row %}
        <th>{{ pair[0] }}</th>
      {% endfor %}
    </tr>
    {% endif %}

    {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
{% endfor %}
</table>

# Scores By Baker
<table style="width: 100%;">
{% for row in site.data.series_15_episode_02_baker_scores %}
    {% if forloop.first %}
    <tr>
      {% for pair in row %}
        <th>{{ pair[0] }}</th>
      {% endfor %}
    </tr>
    {% endif %}

    {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
{% endfor %}
</table>