---
title: "Episode 6 - Floral Week"
layout: post
cover-img: "/assets/images/chocolate_cake.png"
published: false
---
Dana's wishes to get a star baker, handshake, or technical win? Dashed.

# Episode Recap
Floral week sees Dana leaving the tent, with her inconsistent bakes and decorative skill making her the slowest friend running away from the golden bear that is Paul Hollywood. More positively though, Josh *finally* gets the flowers he's been deserving all show, taking the award for Star Baker this week that this author feels has been a long time coming. 

In others news:
- Cristy cries that her bake is ruined, has enough time to _do the broken bit over again to fix it_, and still gets a mention to be Star Baker. Yeah Josh deserved to win this week
- Dan seems to have settled into the midtable. If he makes it to the finals? Not surprised. If he leaves next week? Also not surprised.
- The lovely nans remaining count remains at 1 as Saku survives to see another day in the tent.

# Scores By Team
{% include episode_14_6_scoring_by_team.html %}
# Scores By Baker
{% include episode_14_6_scoring_by_baker.html %}
# Table as of Episode 6
{% include episode_14_6_agg_table.html %}