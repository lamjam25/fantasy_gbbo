---
layout: page
title: Fantasy GBBO
cover-img: "/assets/images/chocolate_cake.png"
---
{% assign episodes = "2,3,4,5,6,7" | split: "," %}
{% assign colNames = "Team 1,Team 1 Score,Team 2,Team 2 Score" | split: "," %}

<h2>Final Table</h2>
<table style="width: 100%;">
    {% for row in site.data.series_15_agg_h2h_table %}
        {% if forloop.first %}
        <tr>
        {% for pair in row %}
            <th>{{ pair[0] }}</th>
        {% endfor %}
        </tr>
        {% endif %}
    
        {% tablerow pair in row %}
        {{ pair[1] }}
        {% endtablerow %}
    {% endfor %}
</table>

<h2>Fixtures Results</h2>
(Yes I'm calling them fixtures. British british british and all that)
{% for episode in episodes %}
<table style="width: 100%;">
Episode {{ episode }}
{% for row in site.data.series_15_agg_fixtures %}
    {% if forloop.first %}
    <tr>
    {% for colName in colNames %}
    <th>{{ colName }}</th>
    {% endfor %}
    </tr>
    {% endif %}
    {% if row["Episode"] == episode %}
    <tr>
    {% for colName in colNames %}
    <td>{{ row[colName] }}</td>
    {% endfor %}
    </tr>
    {% endif %}
{% endfor %}
{% endfor %}
</table>
